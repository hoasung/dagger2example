package dagger.test.hoasung.com.dagger2example.suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import dagger.test.hoasung.com.dagger2example.ExampleInstrumentedTest;

/**
 *  This suite can group some of test class to run together.
 *
 * Created by thu.le on 2/14/2017.
 */

@RunWith(Suite.class)
@Suite.SuiteClasses({ExampleInstrumentedTest.class})
public class UnitTestSuite {
}

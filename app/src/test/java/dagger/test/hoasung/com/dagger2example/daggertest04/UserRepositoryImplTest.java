package dagger.test.hoasung.com.dagger2example.daggertest04;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import dagger.test.hoasung.com.dagger2example.jxretrofit.data.GithubUserRestService;
import dagger.test.hoasung.com.dagger2example.jxretrofit.data.UserRepository;
import dagger.test.hoasung.com.dagger2example.jxretrofit.data.UserRepositoryImpl;
import dagger.test.hoasung.com.dagger2example.jxretrofit.model.User;
import dagger.test.hoasung.com.dagger2example.jxretrofit.model.UsersList;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import rx.observers.TestSubscriber;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test Retrofit, jx, observable directly.
 * <p>
 * Created by thu.le on 2/15/2017.
 */

public class UserRepositoryImplTest {
    private static final String USER_LOGIN_RIGGAROO = "riggarro";
    private String USER_LOGIN_2_REBECCA = "rebecca";

    @Mock
    GithubUserRestService githubUserRestService;

    private UserRepository userRepository;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        userRepository = new UserRepositoryImpl(githubUserRestService);
    }

    @Test
    public void searchUsers_InvokesCorrectApiCalls() {
        //Given
        when(githubUserRestService.searchGithubUsers(anyString())).thenReturn(Observable.just(githubUserList()));

        when(githubUserRestService.getUser(anyString()))
                .thenReturn(Observable.just(user1FullDetails()), Observable.just(user2FullDetails()));


        //When
        TestSubscriber<List<User>> subscriber = new TestSubscriber<>();
        userRepository.searchUsers(USER_LOGIN_RIGGAROO).subscribe(subscriber);

        //Then
        subscriber.awaitTerminalEvent();

        //begin test
        subscriber.assertNoErrors();

        List<List<User>> onNextEvents = subscriber.getOnNextEvents();
        List<User> users = onNextEvents.get(0);

        Assert.assertEquals(USER_LOGIN_RIGGAROO, users.get(0).login);

        verify(githubUserRestService).searchGithubUsers(USER_LOGIN_RIGGAROO);
        //verify(githubUserRestService).searchGithubUsers(USER_LOGIN_2_REBECCA);
    }

    @Test
    public void searchUsers_IOExceptionThenSuccess_SearchUsersRetried() {
        //Given
        when(githubUserRestService.searchGithubUsers(anyString()))
                .thenReturn(getIOExceptionError(), Observable.just(githubUserList()));
        when(githubUserRestService.getUser(anyString()))
                .thenReturn(Observable.just(user1FullDetails()), Observable.just(user2FullDetails()));

        //When
        TestSubscriber<List<User>> subscriber = new TestSubscriber<>();
        userRepository.searchUsers(USER_LOGIN_RIGGAROO).subscribe(subscriber);

        //Then
        subscriber.awaitTerminalEvent();

        //begin test
        subscriber.assertNoErrors();

        verify(githubUserRestService, times(2)).searchGithubUsers(USER_LOGIN_RIGGAROO);

        //verify(githubUserRestService).searchGithubUsers(USER_LOGIN_RIGGAROO);

        verify(githubUserRestService).getUser(USER_LOGIN_RIGGAROO);
        verify(githubUserRestService).getUser(USER_LOGIN_2_REBECCA);
    }

    @Test
    public void searchUsers_GetUserIOExceptionThenSuccess_SearchUsersRetried() {
        //Given
        when(githubUserRestService.searchGithubUsers(anyString())).thenReturn(Observable.just(githubUserList()));

        when(githubUserRestService.getUser(anyString()))
                .thenReturn(getIOExceptionError(), Observable.just(user1FullDetails()),
                        Observable.just(user2FullDetails()));

        //When
        TestSubscriber<List<User>> subscriber = new TestSubscriber<>();
        userRepository.searchUsers(USER_LOGIN_RIGGAROO).subscribe(subscriber);

        //Then
        subscriber.awaitTerminalEvent();
        subscriber.assertNoErrors();

        verify(githubUserRestService, times(2)).searchGithubUsers(USER_LOGIN_RIGGAROO);

        verify(githubUserRestService, times(2)).getUser(USER_LOGIN_RIGGAROO);
        verify(githubUserRestService).getUser(USER_LOGIN_2_REBECCA);
    }

    @Test
    public void searchUsers_OtherHttpError_SearchTerminatedWithError() {
        //Given
        when(githubUserRestService.searchGithubUsers(anyString())).thenReturn(get403ForbiddenError(), Observable.just(githubUserList()));

        //When
        TestSubscriber<List<User>> subscriber = new TestSubscriber<>();
        userRepository.searchUsers(USER_LOGIN_RIGGAROO).subscribe(subscriber);

        //Then
        subscriber.awaitTerminalEvent();

        //Begin test
        subscriber.assertError(HttpException.class);
        //subscriber.assertNoErrors();

        verify(githubUserRestService).searchGithubUsers(USER_LOGIN_RIGGAROO);

        verify(githubUserRestService, never()).getUser(USER_LOGIN_RIGGAROO);
        verify(githubUserRestService, never()).getUser(USER_LOGIN_2_REBECCA);
    }


    private UsersList githubUserList() {
        System.out.println("githubUserList");

        User user = new User();
        user.login = USER_LOGIN_RIGGAROO;

        User user2 = new User();
        user2.login = USER_LOGIN_2_REBECCA;

        List<User> githubUsers = new ArrayList<>();
        githubUsers.add(user);
        githubUsers.add(user2);
        UsersList usersList = new UsersList();
        usersList.items = githubUsers;

        return usersList;
    }

    private User user1FullDetails() {
        System.out.println("user1FullDetails");
        User user = new User();
        user.login = USER_LOGIN_RIGGAROO;
        user.avatarUrl = "avatar_url_1";
        return user;
    }

    private User user2FullDetails() {
        System.out.println("user2FullDetails");
        User user = new User();
        user.login = USER_LOGIN_2_REBECCA;
        user.avatarUrl = "avatar_url_2";
        return user;
    }


    private Observable getIOExceptionError() {
        System.out.println("getIOExceptionError");

        return Observable.error(new IOException());
    }

    private Observable<UsersList> get403ForbiddenError() {
        System.out.println("get403ForbiddenError");
        return Observable.error(new HttpException(
                Response.error(403, ResponseBody.create(MediaType.parse("application/json"), "Forbidden"))));

    }
}

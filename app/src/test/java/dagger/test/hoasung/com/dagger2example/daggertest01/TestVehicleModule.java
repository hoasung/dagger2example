package dagger.test.hoasung.com.dagger2example.daggertest01;

import org.mockito.Mockito;

import dagger.test.hoasung.com.dagger2example.model.Motor;
import dagger.test.hoasung.com.dagger2example.module.VehicleModule;

/**
 * Created by thu.le on 2/14/2017.
 */
/*
 * the simple case to test module in dagger is we create an subclass  that inherits from the parent's module.
 * in this example we test VehicleModule by create new subclass TestVehicleModule.
 *
 * keyword @Module: could not use this class as an module if we want to overwrite the parent's methods.
 * we declare the class without the @Module keyword. if declare it, we will have an error when compile the code
 */
public class TestVehicleModule extends VehicleModule {

    @Override
    public Motor provideMotor() {
        //in the real project, sometime we must mock an instance.
        return Mockito.mock(Motor.class);
    }

    @Override
    public String provideFactoryEmail() {
        return "test3@dagger.com";
    }
}

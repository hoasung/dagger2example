package dagger.test.hoasung.com.dagger2example.daggertest01;

import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import dagger.test.hoasung.com.dagger2example.model.Motor;
import dagger.test.hoasung.com.dagger2example.model.Vehicle;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * use the dagger as normal
 * but we change/custom a module to test and mock data
 *
 * Created by thu.le on 2/14/2017.
 */

public class MainDaggerUnitTest {
    @Inject
    Vehicle myVehicle;

    @Inject
    Motor myMotor;

    @Before
    public void setUp(){
        DaggerTestVehicleComponent.builder().vehicleModule(new TestVehicleModule()).build().inject(this);
    }

    @Test
    public void testPhone(){
        assertThat(myVehicle.getEmail(), not("test1@dagger.com"));
        assertThat(myVehicle.getEmail(), is("test3@dagger.com"));
    }

    @Test
    public void testMotorName(){
        //due to myMotor is mock instance, so we must assume its getName
        when(myMotor.getName()).thenReturn("a motor name");

        assertThat(myVehicle.getName(), is("vehicle:a motor name"));
    }
}

package dagger.test.hoasung.com.dagger2example.daggertest03;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import dagger.test.hoasung.com.dagger2example.component.VehicleComponent;
import dagger.test.hoasung.com.dagger2example.model.Motor;
import dagger.test.hoasung.com.dagger2example.model.Vehicle;
import dagger.test.hoasung.com.dagger2example.module.VehicleModule;
import it.cosenonjaviste.daggermock.DaggerMockRule;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.when;

/**
 * use MockRule, we custom a module and set value for variable myVehicle instead of custom a sub module.
 * in this case we create a sub module dynamic.
 *
 * Created by thu.le on 2/14/2017.
 */

public class MainServiceTestMockitoRule {

    /*
     * use MockRule, we custom a module and set value for variable myVehicle:
     * use the library com.github.fabioCollini:DaggerMock : https://github.com/fabioCollini/DaggerMock
     * In this example the rule dynamically creates a MyModule subclass that returns the mocks defined in the test instead of the real objects.
     *
     * This test is similar to the first test in this project (daggertest01) (the one with InjectMocks annotation),
      * the main difference is that now we are creating mainService field using Dagger
     */
    @Rule public DaggerMockRule<VehicleComponent> mockitoRule =
            new DaggerMockRule<>(VehicleComponent.class, new VehicleModule()).set(new DaggerMockRule.ComponentSetter<VehicleComponent>() {
                @Override
                public void setComponent(VehicleComponent component) {
                    myVehicle = component.provideVehicle();
                    phone = component.aphone();
                }
            });
    @Mock
    Motor myMotor;

    Vehicle myVehicle;

    String phone;


    @Test
    public void testMotorNameByMockitoRule() {
        //due to myMotor is mock instance, so we must assume its getName
        when(myMotor.getName()).thenReturn("a motor name");

        assertThat(myVehicle.getName(), is("vehicle:a motor name"));

        assertThat(phone, is("0123456789"));
    }
}

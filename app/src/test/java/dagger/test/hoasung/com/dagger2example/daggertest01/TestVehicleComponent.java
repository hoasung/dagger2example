package dagger.test.hoasung.com.dagger2example.daggertest01;

import javax.inject.Singleton;

import dagger.Component;
import dagger.test.hoasung.com.dagger2example.component.VehicleComponent;
import dagger.test.hoasung.com.dagger2example.module.VehicleModule;

/**
 * Created by thu.le on 2/14/2017.
 */

/**
 * we need an inject function for class TestDaggerUnitTest
 * so we must create an component TestVehicleComponent that extends from VehicleComponent
 */
@Singleton
@Component(modules = {VehicleModule.class})
public interface TestVehicleComponent extends VehicleComponent {
    void inject(MainDaggerUnitTest handlerUnitTest);
}

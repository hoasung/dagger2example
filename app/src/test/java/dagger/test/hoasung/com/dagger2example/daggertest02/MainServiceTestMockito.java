package dagger.test.hoasung.com.dagger2example.daggertest02;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import dagger.test.hoasung.com.dagger2example.model.Motor;
import dagger.test.hoasung.com.dagger2example.model.Vehicle;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * use InjectMocks to inject fields instead of dagger's component
 *
 * Created by thu.le on 2/14/2017.
 */

public class MainServiceTestMockito {

    /*

    use InjectMocks:

    The MockitoRule usage is equivalent to the MockitoJUnitRunner,
    it invokes the static method MockitoAnnotations.initMocks to populate the annotated fields.
    Thanks to the InjectMocks annotation the myVehicle object is automatically created,
    the Motor defined in the test are used as constructor arguments.
     */
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

//    @Mock
//    @Named("phone") String phone; //will have an error if we define this.
//
//    @Mock
//    @Named("email") String email;  //will have an error if we define this.

    @Mock
    Motor myMotor;

    @InjectMocks
    Vehicle myVehicle;

    @Test
    public void testMotorNameByMockito() {
        //due to myMotor is mock instance, so we must assume its getName
        when(myMotor.getName()).thenReturn("a motor name");

        assertThat(myVehicle.getName(), is("vehicle:a motor name"));
    }
}

package dagger.test.hoasung.com.dagger2example.module;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import dagger.test.hoasung.com.dagger2example.model.Motor;
import dagger.test.hoasung.com.dagger2example.model.Vehicle;
import dagger.test.hoasung.com.dagger2example.scope.HanoiGaraScope;

/**
 * Created by thu.le on 1/23/2017.
 */

@Module
public class HanoiGaraModule {


    /**
     * Note this module is sub component (sub module) of GaraComponent but not the sub component of the VehicleComponent
     * so to use the function of the VehicleComponent, we must declare them clearly in the VehicleComponent
     * @param phone
     * @return
     */
    @Provides
    @HanoiGaraScope
    @Named("location")
    String provideLocation(@Named("phone") String phone) {
        return "2 Ba Trung, Hanoi:" + phone;
    }

    /**
     * Note this module is sub component (sub module) of GaraComponent
     * so to use the @Named("garaname") in the module GaraModule in this module,
     * we don't need declare the @Named("garaname") clearly in the GaraComponenet
     * @param name
     * @return
     */
    @Provides
    @HanoiGaraScope
    @Named("hanoigaraname")
    String provideHNGaraName(@Named("garaname") String name) {
        return "Hanoi gara <--" + name;
    }


}

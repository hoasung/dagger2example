package dagger.test.hoasung.com.dagger2example.jxretrofit.data;

import java.util.List;

import dagger.test.hoasung.com.dagger2example.jxretrofit.model.User;
import rx.Observable;

/**
 * Created by thu.le on 2/15/2017.
 */

public interface UserRepository {
    Observable<List<User>> searchUsers(String searchTerm);
}

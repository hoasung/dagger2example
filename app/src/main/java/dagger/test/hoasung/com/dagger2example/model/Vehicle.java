package dagger.test.hoasung.com.dagger2example.model;

import javax.inject.Inject;

/**
 * Created by thu.le on 1/11/2017.
 */

public class Vehicle {
    private Motor motor;
    private String phone;
    private  String email;


    @Inject
    public Vehicle(){
    }

    public Vehicle(Motor motor, String phone, String email){
        this.motor = motor;
        this.motor.accelerate(90);
        this.email= email;
        this.phone = phone;
    }

    public void increaseSpeed(int value){
        motor.accelerate(value);
    }

    public void stop(){
        motor.brake();
    }

    public int getSpeed(){
        return motor.getRpm();
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getName(){
        return "vehicle:"+motor.getName();
    }
}

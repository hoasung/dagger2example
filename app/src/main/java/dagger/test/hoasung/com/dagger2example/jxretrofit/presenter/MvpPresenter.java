package dagger.test.hoasung.com.dagger2example.jxretrofit.presenter;

import dagger.test.hoasung.com.dagger2example.jxretrofit.view.MvpView;

public interface MvpPresenter<V extends MvpView> {

    void attachView(V mvpView);

    void detachView();
}


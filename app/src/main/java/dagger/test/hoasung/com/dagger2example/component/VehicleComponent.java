package dagger.test.hoasung.com.dagger2example.component;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Component;
import dagger.test.hoasung.com.dagger2example.MainActivity;
import dagger.test.hoasung.com.dagger2example.model.Vehicle;
import dagger.test.hoasung.com.dagger2example.module.VehicleModule;

/**
 * Created by thu.le on 1/11/2017.
 */

@Singleton
@Component(modules = {VehicleModule.class})
public interface VehicleComponent {
    /**
     * we can define function here so client can use without calling inject function.
     * we don't need parameters, just type of return and its name.
     * type of return must be one of types in the module VehicleModule class
     */
    Vehicle provideVehicle();

    /**
     * we must define the inject function
     * to tell the dagger search variables in the MainActivity when inject
     * @param activity
     */
    void inject(MainActivity activity);

    @Named("phone") String aphone();
}

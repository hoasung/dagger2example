package dagger.test.hoasung.com.dagger2example.jxretrofit.presenter;


import java.util.List;

import dagger.test.hoasung.com.dagger2example.jxretrofit.model.User;
import dagger.test.hoasung.com.dagger2example.jxretrofit.view.MvpView;


public interface UserSearchContract {

    interface View extends MvpView {
        void showSearchResults(List<User> githubUserList);

        void showError(String message);

        void showLoading();

        void hideLoading();
    }

    interface Presenter extends MvpPresenter<View> {
        void search(String term);
    }
}

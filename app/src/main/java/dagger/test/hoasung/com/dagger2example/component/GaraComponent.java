package dagger.test.hoasung.com.dagger2example.component;

import dagger.Component;
import dagger.test.hoasung.com.dagger2example.MainActivity;
import dagger.test.hoasung.com.dagger2example.model.Person;
import dagger.test.hoasung.com.dagger2example.module.GaraModule;
import dagger.test.hoasung.com.dagger2example.module.HanoiGaraModule;
import dagger.test.hoasung.com.dagger2example.scope.UserScope;

/**
 * Created by thu.le on 1/19/2017.
 */

@UserScope
@Component(dependencies = VehicleComponent.class, modules = GaraModule.class)
public interface GaraComponent {
    Person getManager();

    void inject(MainActivity activity);

    HanoiGaraSubcomponent newHanoiGaraSubcomponent(HanoiGaraModule module);


    //We don't need declare dowan stream function garaname in the module GaraModule as below
    //because HanoiGaraSubcomponent is sub component.
    //but if we want to use the function in the VehicleComponent from in HanoiGaraSubcomponent, we must declare it explicitly
    //@Named("garaname")
    //String privdeGaraname()
}

package dagger.test.hoasung.com.dagger2example.scope;

import javax.inject.Scope;

/**
 * Created by thu.le on 1/18/2017.
 */

@Scope
public @interface ActivityScope {
}

package dagger.test.hoasung.com.dagger2example.jxretrofit.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by thu.le on 2/15/2017.
 */

public class User {

    @SerializedName("login")
    @Expose
    public String login;

    @SerializedName("id")
    @Expose
    public Integer id;

    @SerializedName("avatar_url")
    @Expose
    public String avatarUrl;

    @SerializedName("gravatar_id")
    @Expose
    public String gravatarId;

    public String bio;
    public String name;


    public User() {
    }

    public User(final String userLogin, final String name, final String avatarUrl, final String bio) {
        this.login = userLogin;
        this.name = name;
        this.avatarUrl = avatarUrl;
        this.bio = bio;
    }
}

package dagger.test.hoasung.com.dagger2example.component;

import javax.inject.Named;

import dagger.Module;
import dagger.Subcomponent;
import dagger.test.hoasung.com.dagger2example.module.HanoiGaraModule;
import dagger.test.hoasung.com.dagger2example.scope.HanoiGaraScope;

/**
 * Created by thu.le on 1/23/2017.
 */

@HanoiGaraScope
@Subcomponent(modules = HanoiGaraModule.class)
public interface HanoiGaraSubcomponent {
    @Named("location")
    String location();

    @Named("hanoigaraname")
    String garaName();
}

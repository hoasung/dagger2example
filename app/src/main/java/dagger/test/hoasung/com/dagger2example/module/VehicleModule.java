package dagger.test.hoasung.com.dagger2example.module;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.test.hoasung.com.dagger2example.model.Motor;
import dagger.test.hoasung.com.dagger2example.model.Vehicle;

/**
 * Created by thu.le on 1/11/2017.
 */

@Module
public class VehicleModule {
    @Provides
    @Singleton
    public Motor provideMotor() {
        Motor a = new Motor("a motor name");

        return a;
    }

    /**
     * when we define an inject variable of Vehicle in an activity, fragment or any view
     * <p>
     * Example in your activity, we declare:
     *
     * @param motor
     * @param phone due we use two String types in the parameters we must use the @Named to identify
     * @param email
     * @return
     * @Inject Vehicle myVehicle;
     * <p>
     * Then Dagger actually will generate a real code like this:
     * <p>
     * myVehicle = provideVehicle(provideMotor(), provideFactoryPhone(), provideFactoryEmail());
     * <p>
     * Due to the function need 3 parameters: motor, phone, email.
     * <p>
     * When it need a motor, it will search function with signals @Provides Motor ...
     * then the function provideMotor is found out and pass it for the function provideVehicle
     * <p>
     * When it need a phone , it will search function with with signal @Named("phone") String ...
     * then the function provideFactoryPhone is found out and pass it for the function provideVehicle
     * <p>
     * steps the same for email parameters.
     * <p>
     * If we don't provide these functions, dagger will show error when we compile the source code.
     */
    @Provides
    @Singleton
    public Vehicle provideVehicle(Motor motor, @Named("phone") String phone, @Named("email") String email) {
        return new Vehicle(motor, phone, email);
    }

    @Provides
    @Singleton
    @Named("phone")
    public String provideFactoryPhone() {
        return "0123456789";
    }

    @Provides
    @Singleton
    @Named("email")
    public String provideFactoryEmail() {
        return "test@dagger.com";
    }


}

package dagger.test.hoasung.com.dagger2example.module;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import dagger.test.hoasung.com.dagger2example.model.Person;
import dagger.test.hoasung.com.dagger2example.model.Vehicle;
import dagger.test.hoasung.com.dagger2example.scope.UserScope;

/**
 * Created by thu.le on 1/19/2017.
 */

@Module
public class GaraModule {

    @Provides
    @UserScope
    Person provideManager(Vehicle vehicle){
        return new Person(vehicle);
    }

    @Provides
    @UserScope
    @Named("garaname")
    String privdeGaraname(){
        return "GaraModule";
    }
}

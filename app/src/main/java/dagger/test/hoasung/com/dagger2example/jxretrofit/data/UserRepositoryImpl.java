package dagger.test.hoasung.com.dagger2example.jxretrofit.data;

import java.io.IOException;
import java.util.List;

import dagger.test.hoasung.com.dagger2example.jxretrofit.model.User;
import dagger.test.hoasung.com.dagger2example.jxretrofit.model.UsersList;
import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import rx.functions.Func0;
import rx.functions.Func1;

/**
 * Created by thu.le on 2/15/2017.
 */

public class UserRepositoryImpl implements UserRepository {
    private GithubUserRestService githubUserRestService;

    public UserRepositoryImpl(GithubUserRestService githubUserRestService) {
        this.githubUserRestService = githubUserRestService;
    }


    @Override
    public Observable<List<User>> searchUsers(final String searchTerm) {

        return Observable.defer(new Func0<Observable<List<User>>>() {
            @Override
            public Observable<List<User>> call() {
                return githubUserRestService.searchGithubUsers(searchTerm).concatMap(
                        new Func1<UsersList, Observable<? extends List<User>>>() {
                    @Override
                    public Observable<? extends List<User>> call(UsersList usersList) {
                        return Observable.from(usersList.items)
                                .concatMap(new Func1<User, Observable<? extends User>>() {
                            @Override
                            public Observable<? extends User> call(User user) {
                                return githubUserRestService.getUser(user.login);
                            }
                        }).toList();
                    }
                });
            }
        }).retryWhen(new Func1<Observable<? extends Throwable>, Observable<?>>() {
            @Override
            public Observable<?> call(Observable<? extends Throwable> observable) {
                //return observable.flatMap(new Func1<Throwable, Observable<?>>() {
                // if we use ?, not works. so we must declare it explicitly
                return observable.flatMap(new Func1<Throwable, Observable<Throwable>>() {
                    @Override
                    public Observable<Throwable> call(Throwable o) {
                        //System.out.println("call:retryWhen:" + o.getClass().getSimpleName());

                        if (o instanceof IOException) {
                            return Observable.just(null);
                        }
//                        else if( o instanceof HttpException){
//                            return Observable.just(null);
//                        }

                        return Observable.error(o);
                    }
                });
            }
        });
    }
}

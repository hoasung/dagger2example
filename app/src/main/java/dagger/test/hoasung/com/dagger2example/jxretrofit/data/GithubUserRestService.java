package dagger.test.hoasung.com.dagger2example.jxretrofit.data;

import dagger.test.hoasung.com.dagger2example.jxretrofit.model.User;
import dagger.test.hoasung.com.dagger2example.jxretrofit.model.UsersList;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by thu.le on 2/15/2017.
 */

public interface GithubUserRestService {

    @GET("/search/users?per_page=2")
    Observable<UsersList> searchGithubUsers(@Query("q") String searchTerm);

    @GET("/users/{username}")
    Observable<User> getUser(@Path("username") String username);
}

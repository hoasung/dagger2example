package dagger.test.hoasung.com.dagger2example.jxretrofit.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thu.le on 2/15/2017.
 */

public class UsersList {
    @SerializedName("total_count")
    @Expose
    public Integer totalCount;


    @SerializedName("items")
    @Expose
    public List<User> items = new ArrayList<User>();

    public UsersList(){
    }
    public UsersList(final List<User> githubUsers) {
        this.items = githubUsers;
    }

}

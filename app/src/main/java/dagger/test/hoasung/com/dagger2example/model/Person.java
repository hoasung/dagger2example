package dagger.test.hoasung.com.dagger2example.model;

import javax.inject.Inject;

import dagger.test.hoasung.com.dagger2example.scope.UserScope;

/**
 * Created by thu.le on 1/19/2017.
 */

@UserScope
public class Person {
    private final Vehicle vehicle;

    @Inject
    public Person(Vehicle vehicle){
        this.vehicle = vehicle;
    }


    public Vehicle getVehicle() {
        return vehicle;
    }
}

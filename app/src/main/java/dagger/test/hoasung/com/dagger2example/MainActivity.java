package dagger.test.hoasung.com.dagger2example;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import javax.inject.Inject;

import dagger.test.hoasung.com.dagger2example.component.DaggerGaraComponent;
import dagger.test.hoasung.com.dagger2example.component.DaggerVehicleComponent;
import dagger.test.hoasung.com.dagger2example.component.GaraComponent;
import dagger.test.hoasung.com.dagger2example.component.VehicleComponent;
import dagger.test.hoasung.com.dagger2example.model.Vehicle;
import dagger.test.hoasung.com.dagger2example.module.GaraModule;
import dagger.test.hoasung.com.dagger2example.module.HanoiGaraModule;
import dagger.test.hoasung.com.dagger2example.module.VehicleModule;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    /**
     * we declare a variable myVehicle, we don't care about how to create it.
     * we just use the component to inject value for this variable.
     */
    @Inject
    Vehicle myVehicle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //A. Test auto injected:

        TextView textView = (TextView) findViewById(R.id.textView);

        String logText = "Dagger 2 Example.\n\n";

        //check value before calling inject function
        logText += "Before call Inject: myVehicle is null? " + (myVehicle==null);

        //call the function inject of component to inject values automatically for all variables in this class.
        VehicleComponent vehicleComponent = DaggerVehicleComponent.builder().vehicleModule(new VehicleModule()).build();
        vehicleComponent .inject(this);

        //check value after call inject function
        logText += "\n\nAfter calling inject: myVehicle is null? " + (myVehicle==null);

        //try to call function in the variable
        logText += "\n\nmyVehicle.email is " + (myVehicle.getEmail());


        //show info
        Log.d(TAG, logText);
        textView.setText(logText);


        //B. Test dependencies and auto injected:
        //Test dependency component: GaraComponent depend on VehicleComponent, so we must set vehicleComponent to create it.
        GaraComponent garaComponent = DaggerGaraComponent.builder().vehicleComponent(vehicleComponent).garaModule(new GaraModule()).build();
        Log.d(TAG, "person:" + garaComponent.getManager().getVehicle().getEmail());

        //C. Test sub component

        String location  = garaComponent.newHanoiGaraSubcomponent(new HanoiGaraModule()).location();
        Log.d(TAG, "sub-component-location:" + location);

        String hnname  = garaComponent.newHanoiGaraSubcomponent(new HanoiGaraModule()).garaName();
        Log.d(TAG, "sub-component-location:" + hnname);
    }
}

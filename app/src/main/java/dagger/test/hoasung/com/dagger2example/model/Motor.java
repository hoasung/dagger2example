package dagger.test.hoasung.com.dagger2example.model;

import javax.inject.Inject;

/**
 * Created by thu.le on 1/11/2017.
 */

public class Motor {
    private int rpm;
    private String name;

    //@Inject: due to we are using the MockitoRule to test VehicleModule so @Inject keyword will not work here.
    public Motor(String name){
        this.rpm = 0;
        this.name = name;
    }

    public int getRpm(){
        return rpm;
    }

    public void accelerate(int value){
        rpm = rpm + value;
    }

    public void brake(){
        rpm = 0;
    }

    public String getName(){return name;};
}
